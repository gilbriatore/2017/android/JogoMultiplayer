package br.edu.up.jogomultiplayer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import static br.edu.up.jogomultiplayer.PlayerActivity.TAG;

public class ServerAsyncTask extends AsyncTask<String, String, String> {

  private Context context;
  private final int PORTA = 8888;
  private BufferedReader bufferedReader;
  private BufferedWriter bufferedWriter;

  public ServerAsyncTask(Context context) {
    this.context = context;
  }

  @Override
  protected String doInBackground(String... params) {

    Socket socket = new Socket();
    String retorno = null;
    try {

      String mensagem = params[0];

      Log.d(TAG, "ServerAsyncTask iniciando servidor!");

      ServerSocket serverSocket = new ServerSocket(PORTA);
      socket = serverSocket.accept();

      OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
      bufferedWriter = new BufferedWriter(osw);

      InputStreamReader isr = new InputStreamReader(socket.getInputStream());
      bufferedReader = new BufferedReader(isr);

      if (mensagem != null) {
        //Log.d(TAG, "ServerAsyncTask DataOutputStream...");
        //dos = new DataOutputStream(socket.getOutputStream());
        //dos.writeUTF(mensagem);
        //mensagem = null;
        bufferedWriter.write(mensagem);
        Log.d(TAG, "ServerAsyncTask mensagem enviada!");
      }

      //Log.d(TAG, "ServerAsyncTask DataInputStream...");
      //dis = new DataInputStream(socket.getInputStream());
      //String msg = dis.readUTF();
      retorno = bufferedReader.readLine();


      //Log.d(TAG, "ServerAsyncTask mensagem recebida: " + msg);

    } catch (IOException e) {
      e.printStackTrace();
    } finally {

//      if (dis != null){
//        try {
//          dis.close();
//        } catch (IOException e) {
//          e.printStackTrace();
//        }
//      }
//
//      if (dos != null) {
//        try {
//          dos.close();
//        } catch (IOException e) {
//          e.printStackTrace();
//        }
//      }

      if (socket != null) {
        if (socket.isConnected()) {
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

    }
    return retorno;
  }

  @Override
  protected void onPostExecute(String mensagem) {
    Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
  }

//  public BufferedWriter getBufferedWriter() {
//    return bufferedWriter;
//  }
//
//  public void setBufferedWriter(BufferedWriter bufferedWriter) {
//    this.bufferedWriter = bufferedWriter;
//  }
//
//  public BufferedReader getBufferedReader() {
//    return bufferedReader;
//  }
//
//  public void setBufferedReader(BufferedReader bufferedReader) {
//    this.bufferedReader = bufferedReader;
//  }
}