package br.edu.up.jogomultiplayer;

import java.io.BufferedWriter;
import java.io.IOException;

import static android.util.Log.d;
import static br.edu.up.jogomultiplayer.PlayerActivity.TAG;

public class Mensageiro implements Runnable {

  private String mensagem;
  private BufferedWriter writer;

  public Mensageiro(BufferedWriter writer, String mensagem) {
    this.mensagem = mensagem;
    this.writer = writer;
  }

  @Override
  public void run() {
    if (writer != null) {
      try {
        writer.write(mensagem);
        d(TAG, "Mensageiro: Mensagem enviada!");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
