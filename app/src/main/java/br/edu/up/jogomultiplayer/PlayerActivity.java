package br.edu.up.jogomultiplayer;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION;
import static android.util.Log.d;

public class PlayerActivity extends AppCompatActivity {

  private ArrayList<WifiP2pDevice> dispositivos;
  private ArrayList<String> nomesDosDispositivos;
  private ArrayAdapter<String> adapter;
  private ListView listView;
  public static String TAG = "JogoMultiplayer";

  private class ChannelListener implements WifiP2pManager.ChannelListener {

    @Override
    public void onChannelDisconnected() {
      // Reinicia o canal de comunicação;
      channel = wifiP2pManager.initialize(PlayerActivity.this, getMainLooper(), this);
    }
  }

  ImageView imgLinha;
  ArrayList<View> listaDeViews;

  WifiP2pManager wifiP2pManager;
  WifiP2pManager.Channel channel;
  WiFiDirectBroadcastReceiver broadcastReceiver;
  IntentFilter intentFilter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_player);

    listaDeViews = new ArrayList<>();
    LinearLayout linha1 = (LinearLayout) findViewById(R.id.linha1);
    LinearLayout linha2 = (LinearLayout) findViewById(R.id.linha2);
    LinearLayout linha3 = (LinearLayout) findViewById(R.id.linha3);
    listaDeViews.addAll(linha1.getTouchables());
    listaDeViews.addAll(linha2.getTouchables());
    listaDeViews.addAll(linha3.getTouchables());
    imgLinha = (ImageView) findViewById(R.id.imgLinha);

    nomesDosDispositivos = new ArrayList<>();
    nomesDosDispositivos.add("Listando dispositivos...");
    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, nomesDosDispositivos);
    listView = (ListView) findViewById(R.id.listView);
    listView.setAdapter(adapter);
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (dispositivos != null && dispositivos.size() > 0) {
          WifiP2pDevice dispositivo = dispositivos.get(position);
          conectarDispositivo(dispositivo);
        }
      }
    });


    // Filtro de acordo com as ações necessárias;
    intentFilter = new IntentFilter();
    intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
    intentFilter.addAction(WIFI_P2P_PEERS_CHANGED_ACTION);
    intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
    intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

    // Gerenciador da rede Wi-Fi;
    wifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);

    // Canal de comunicação;
    channel = wifiP2pManager.initialize(this, getMainLooper(), new ChannelListener());
  }

  public void conectarDispositivo(WifiP2pDevice dispositivo) {

    WifiP2pConfig config = new WifiP2pConfig();
    config.deviceAddress = dispositivo.deviceAddress;
    config.wps.setup = WpsInfo.PBC;

    // Após a conexão invocda  WIFI_P2P_CONNECTION_CHANGED_ACTION;
    wifiP2pManager.connect(channel, config, new WifiP2pManager.ActionListener() {
      @Override
      public void onSuccess() {
        mostrarMensagem("Conexão iniciada...");
      }

      @Override
      public void onFailure(int reason) {
        mostrarMensagem("Falha na conexão!");
      }
    });

  }

  @Override
  protected void onResume() {
    super.onResume();
    // Registra receiver para transmissão e recebimento das mensagens;
    broadcastReceiver = new WiFiDirectBroadcastReceiver(this, wifiP2pManager, channel);
    registerReceiver(broadcastReceiver, intentFilter);

    buscarDispositivos();
  }

  private void buscarDispositivos() {

    // Após a busca assíncrona chama a função WIFI_P2P_PEERS_CHANGED_ACTION;
    wifiP2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
      @Override
      public void onSuccess() {
        mostrarMensagem("Busca iniciada...");
      }

      @Override
      public void onFailure(int reason) {
        mostrarMensagem("Falha na busca!");
      }
    });
  }

  @Override
  protected void onPause() {
    super.onPause();
    // Remove registro do receiver;
    unregisterReceiver(broadcastReceiver);
  }

  public void listarDispositivos(ArrayList<WifiP2pDevice> dispositivos) {
    this.dispositivos = dispositivos;

    nomesDosDispositivos = new ArrayList<>();

    if (dispositivos != null) {
      for (int i = 0; i < dispositivos.size(); i++) {
        WifiP2pDevice dispositivo = dispositivos.get(i);
        String dados = dispositivo.deviceName;
        if (dispositivo.status == WifiP2pDevice.CONNECTED) {
          dados += " (Conectado)";
        }
        nomesDosDispositivos.add(dados);
      }
    } else {
      nomesDosDispositivos.add("Nenhum dispositivo encontrado!");
    }
    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, nomesDosDispositivos);
    listView.setAdapter(adapter);
  }


  public void onClickAtualizar(View v) {
    buscarDispositivos();
  }

  private void mostrarMensagem(String msg) {
    Toast.makeText(PlayerActivity.this, msg, Toast.LENGTH_SHORT).show();
  }

  /**
   * código do jogo
   **/

  public void onClickReiniciar(View v) {

    imgLinha.setImageResource(R.drawable.fundo_transparente);
    for (View view : listaDeViews) {
      ImageView imageView = (ImageView) view;
      imageView.setImageResource(R.drawable.btn_branco);
      imageView.setEnabled(true);

    }
    limparJogadas();
  }

  char jogadorDaVez = 'o';
  char[] jogadas = new char[9];
  {
    limparJogadas();
  }
  int[][] sequencias = {
      {1, 2, 3}, {4, 5, 6}, {7, 8, 9},
      {1, 4, 7}, {2, 5, 8}, {3, 6, 9},
      {1, 5, 9}, {3, 5, 7}
  };

  public void limparJogadas(){
    for (int i = 0; i < jogadas.length; i++) {
      jogadas[i] = '-';
    }
  }


  Map<Integer, Integer> linhas = new HashMap<>();

  {
    linhas.put(0, R.drawable.linha_horizontal_1);
    linhas.put(1, R.drawable.linha_horizontal_2);
    linhas.put(2, R.drawable.linha_horizontal_3);
    linhas.put(3, R.drawable.linha_vertical_1);
    linhas.put(4, R.drawable.linha_vertical_2);
    linhas.put(5, R.drawable.linha_vertical_3);
    linhas.put(6, R.drawable.linha_decrescente);
    linhas.put(7, R.drawable.linha_crescente);
  }

  public void onClickJogar(View v) {

    ImageView imageView = (ImageView) v;
    imageView.setEnabled(false);
    String tag = (String) imageView.getTag();
    int posicao = Integer.parseInt(tag);
    jogadas[posicao - 1] = jogadorDaVez;

    for (int i = 0; i < sequencias.length; i++) {

      int[] sequencia = sequencias[i];
      if (jogadas[sequencia[0] - 1] == jogadorDaVez &&
          jogadas[sequencia[1] - 1] == jogadorDaVez &&
          jogadas[sequencia[2] - 1] == jogadorDaVez) {

        for (View view : listaDeViews) {
          ImageView imgv = (ImageView) view;
          imgv.setEnabled(false);
        }

        imgLinha.setImageResource(linhas.get(i));
      }
    }

    if (jogadorDaVez == 'o') {
      imageView.setImageResource(R.drawable.o_verde_hdpi);
      jogadorDaVez = 'x';
    } else {
      imageView.setImageResource(R.drawable.x_preto_hdpi);
      jogadorDaVez = 'o';
    }

    //broadcastReceiver.jogar(jogadas);
    WifiP2pInfo wifiP2pInfo = broadcastReceiver.getWifiP2pInfo();
    if (wifiP2pInfo != null) {
      //MensageiroTask mensageiroTask = new MensageiroTask(this, wifiP2pInfo);
      //mensageiroTask.execute(new String(jogadas));




    } else {
      Toast.makeText(this, "Não foi possível jogar!", Toast.LENGTH_SHORT).show();
    }
  }

  /**********************************************************************/



  public static class MensageiroTask extends AsyncTask<String, String, String> {

    private WifiP2pInfo wifiP2pInfo;
    private final int PORTA = 8888;
    private final int SOCKET_TIMEOUT = 5000;
    private Context context;

    public MensageiroTask(Context context, WifiP2pInfo wifiP2pInfo) {
      this.wifiP2pInfo = wifiP2pInfo;
    }

    @Override
    protected void onPreExecute() {
      d(TAG, "MensageiroTask iniciado!");
    }

    @Override
    protected String doInBackground(String... params) {

      d(TAG, "doInBackground iniciado!");
      ServerSocket serverSocket = null;
      Socket socket = new Socket();
      BufferedReader bufferedReader = null;
      BufferedWriter bufferedWriter = null;
      String retorno = null;

      try {
        if (wifiP2pInfo.isGroupOwner) {
          serverSocket = new ServerSocket(PORTA);
          socket = serverSocket.accept();
        } else {
          socket.bind(null);
          String host = wifiP2pInfo.groupOwnerAddress.getHostAddress();
          InetSocketAddress address = new InetSocketAddress(host, PORTA);
          socket.connect(address, SOCKET_TIMEOUT);
        }

        d(TAG, "Socket conectado: " + socket.isConnected());

        d(TAG, "Mensagem: " + params[0]);

        OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
        bufferedWriter = new BufferedWriter(osw);
        bufferedWriter.write(params[0]);

        InputStreamReader isr = new InputStreamReader(socket.getInputStream());
        bufferedReader = new BufferedReader(isr);
        retorno = bufferedReader.readLine();

        d(TAG, "Retorno: " + retorno);

      } catch (IOException e) {
        e.printStackTrace();
      } finally {

        if (bufferedReader != null) {
          try {
            bufferedReader.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (bufferedWriter != null) {
          try {
            bufferedWriter.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (socket != null) {
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }

        if (serverSocket != null) {
          try {
            serverSocket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      return retorno;
    }

    @Override
    protected void onPostExecute(String retorno) {
      if (retorno != null) {
        Toast.makeText(context, retorno, Toast.LENGTH_SHORT).show();
      } else {
        Toast.makeText(context, "Nenhum retorno!", Toast.LENGTH_SHORT).show();
      }
    }
  }
}