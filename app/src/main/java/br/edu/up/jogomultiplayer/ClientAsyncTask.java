package br.edu.up.jogomultiplayer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import static br.edu.up.jogomultiplayer.PlayerActivity.TAG;

public class ClientAsyncTask extends AsyncTask<Void, Void, String> {

  private Context context;
  private InetAddress inetAddress;
  private final int PORTA = 8888;
  private static final int SOCKET_TIMEOUT = 5000;
  private String mensagem = "Mensagem Hardcoded no ClientAsynctack!";
  private BufferedReader bufferedReader;
  private BufferedWriter bufferedWriter;


  public ClientAsyncTask(Context context, InetAddress inetAddress, String  mensagem) {
    this.context = context;
    this.inetAddress = inetAddress;
    this.mensagem = mensagem;
  }

  @Override
  protected String doInBackground(Void... params) {

    Socket socket = new Socket();
    DataOutputStream dos = null;
    DataInputStream dis = null;

    try {

      Log.d(TAG, "ClientAsyncTask abrindo conexão com servidor!");

      socket.bind(null);
      socket.connect((new InetSocketAddress(inetAddress, PORTA)), SOCKET_TIMEOUT);

      Log.d(TAG, "ClientAsyncTask está conectado: " + socket.isConnected());

      OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
      bufferedWriter = new BufferedWriter(osw);

      InputStreamReader isr = new InputStreamReader(socket.getInputStream());
      bufferedReader = new BufferedReader(isr);

      if (mensagem != null) {

//        Log.d(TAG, "ClientAsyncTask DataOutputStream...");
//        dos = new DataOutputStream(socket.getOutputStream());
//        dos.writeUTF(mensagem);
        //mensagem = null;
        bufferedWriter.write(mensagem);
        Log.d(TAG, "ClientAsyncTask mensagem enviada!");
      }


//      Log.d(TAG, "ClientAsyncTask DataInputStream...");
//      dis = new DataInputStream(socket.getInputStream());
//      String msg = dis.readUTF();

      String msg = bufferedReader.readLine();
      Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
      //Log.d(TAG, "ClientAsyncTask mensagem recebida: " + msg);

    } catch (IOException e) {

      Log.e(TAG, e.getMessage());

    } finally {

      if (dis != null){
        try {
          dis.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

      if (dos != null) {
        try {
          dos.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

      if (socket != null) {
        if (socket.isConnected()) {
          try {
            socket.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return null;
  }

  public BufferedReader getBufferedReader() {
    return bufferedReader;
  }

  public void setBufferedReader(BufferedReader bufferedReader) {
    this.bufferedReader = bufferedReader;
  }

  public BufferedWriter getBufferedWriter() {
    return bufferedWriter;
  }

  public void setBufferedWriter(BufferedWriter bufferedWriter) {
    this.bufferedWriter = bufferedWriter;
  }
}