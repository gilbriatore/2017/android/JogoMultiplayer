package br.edu.up.jogomultiplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.provider.Settings;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import static android.util.Log.d;
import static br.edu.up.jogomultiplayer.PlayerActivity.TAG;

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

  //Connecting Devices Wirelessly
  //Using Network Service Discovery
  //Discover Services on the Network
  //Connect to Services on the Network
  //Unregister Your Service on Application Close

  //https://developer.android.com/guide/topics/connectivity/wifip2p.html#creating-app
  //https://developer.android.com/training/connect-devices-wirelessly/wifi-direct.html

  private PlayerActivity activity;
  private WifiP2pManager wifiP2pManager;
  private WifiP2pManager.Channel channel;
  private WifiP2pInfo wifiP2pInfo;

  public WiFiDirectBroadcastReceiver(PlayerActivity activity,
                                     WifiP2pManager wifiP2pManager, WifiP2pManager.Channel channel) {
    super();
    this.activity = activity;
    this.wifiP2pManager = wifiP2pManager;
    this.channel = channel;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();

    if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

      d(TAG, "WIFI_P2P_STATE_CHANGED_ACTION");
      // Verifica se a Wi-Fi está ativada e notifica a activity;
      int status = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
      if (status != WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
        Intent intentWifi = new Intent(Settings.ACTION_WIFI_SETTINGS);
        activity.startActivity(intentWifi);
        Toast.makeText(activity, "Ative a Wi-Fi e pressione voltar!", Toast.LENGTH_LONG).show();
      }

    } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

      d(TAG, "WIFI_P2P_PEERS_CHANGED_ACTION");
      wifiP2pManager.requestPeers(channel, new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peers) {

          if (peers.getDeviceList().size() > 0) {
            ArrayList<WifiP2pDevice> dispositivos = new ArrayList<>(peers.getDeviceList());
            activity.listarDispositivos(dispositivos);
          } else {
            activity.listarDispositivos(null);
          }

        }
      });

    } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

      d(TAG, "WIFI_P2P_CONNECTION_CHANGED_ACTION");
      NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
      d(TAG, "WiFiDirectBroadcastReceiver isConectado: " + networkInfo.isConnected());

      if (networkInfo != null && networkInfo.isConnected()) {

        wifiP2pManager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
          @Override
          public void onConnectionInfoAvailable(WifiP2pInfo wInfo) {
            wifiP2pInfo = wInfo;

            if (!isDispositivoConectado) {
              isDispositivoConectado = true;
              if (wifiP2pInfo.isGroupOwner) {
                new Thread(new WiFiDirectBroadcastReceiver.ServidorThread()).run();
              } else {
                new Thread(new WiFiDirectBroadcastReceiver.ClienteThread()).run();
              }
            }
          }
        });
      } else {
        d(TAG, "WiFiDirectBroadcastReceiver sem conexão!");
        isDispositivoConectado = false;
      }
    } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
      // Responde ao dispositivo a mundança de status da wifi;
      d(TAG, "WIFI_P2P_THIS_DEVICE_CHANGED_ACTION");
    }
  }

  public WifiP2pInfo getWifiP2pInfo() {
    return wifiP2pInfo;
  }


  Handler handler = new Handler();
  final static int PORTA = 8888;
  String mensagemEnvio = "Mensagem hardcoded";
  String mensagemRetorno;
  boolean isDispositivoConectado;


  public class ServidorThread implements Runnable {

    @Override
    public void run() {
      try {
        while (isDispositivoConectado) {
          ServerSocket serverSocket = new ServerSocket(PORTA);
          d(TAG, "ServidorThread aguardando conexão!");

          Socket socket = serverSocket.accept();
          d(TAG, "ServidorThread conectado!");

          DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
          DataInputStream dis = new DataInputStream(socket.getInputStream());

          dos.writeBytes(mensagemEnvio);
          d(TAG, "ServidorThread mensagem enviada!");

          mensagemRetorno = dis.readUTF();
          d(TAG, "ServidorThread mensagem recebida: " + mensagemRetorno);

          handler.post(new Runnable() {
            @Override
            public void run() {
              Toast.makeText(activity, mensagemRetorno, Toast.LENGTH_SHORT).show();
            }
          });

          socket.close();
          Thread.sleep(100);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public class ClienteThread implements Runnable {

    @Override
    public void run() {
      try {
        while (isDispositivoConectado) {
          InetAddress inetAddress = wifiP2pInfo.groupOwnerAddress;

          Socket socket = new Socket(inetAddress, PORTA);
          d(TAG, "ClienteThread conectado!");

          DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
          DataInputStream dis = new DataInputStream(socket.getInputStream());

          dos.writeBytes(mensagemEnvio);
          d(TAG, "ClienteThread mensagem enviada!");

          mensagemRetorno = dis.readUTF();
          d(TAG, "ClienteThread mensagem recebida: " + mensagemRetorno);

          handler.post(new Runnable() {
            @Override
            public void run() {
              Toast.makeText(activity, mensagemRetorno, Toast.LENGTH_SHORT).show();
            }
          });

          socket.close();
          Thread.sleep(100);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}